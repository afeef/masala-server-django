# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from . import views

urlpatterns = [
    url(
        regex=r'^channels/(?P<channel_id>.+)/shows/$',
        view=views.ShowList.as_view(),
        name='shows_api'
    ),
    url(
        regex=r'^channels/(?P<channel_id>.+)/episodes/$',
        view=views.EpisodeList.as_view(),
        name='episodes_api'
    ),
    url(
        regex=r'^shows/(?P<show_id>.+)/episodes/$',
        view=views.EpisodeList.as_view(),
        name='episodes_api'
    ),
    url(
        regex=r'^channels/$',
        view=views.ChannelList.as_view(),
        name='channels_api'
    ),
    url(
        regex=r'^shows/$',
        view=views.ShowList.as_view(),
        name='shows_api'
    ),
    url(
        regex=r'^episodes/$',
        view=views.EpisodeList.as_view(),
        name='episodes_api'
    ),
]
