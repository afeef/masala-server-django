from dateutil import parser
from django.core.management.base import BaseCommand
from django.db import transaction
from rest_framework import status
from rest_framework.test import APIClient

from common.models import Channel, Show, Episode


@transaction.atomic
class Command(BaseCommand):
    """
    Usage: python manage.py import_videos UClkIneW_r2Sp2mjdFt2X4rg
    """
    args = '<channel_id>'
    help = 'Imports all play lists of the given channel_id'

    def handle(self, *args, **options):
        for channel_id in args:
            self.stdout.write('Reading channel {channel_id}'.format(
                channel_id=channel_id
            ))

            if Channel.objects.filter(youtube_id=channel_id).exists():
                channel = Channel.objects.get(youtube_id=channel_id)
                client = APIClient()
                shows = Show.objects.filter(channel=channel)

                for show in shows:
                    self.save_page(client=client, show=show)
            else:
                self.stdout.write('Channel "{channel_id}" not exists.'.format(channel_id=channel.youtube_id))

    def save_page(self, client, show, next_page_token=''):

        response = client.get('/youtube/play-lists/{play_list_id}/playlist-items/?page_token={page_token}'.format(
            play_list_id=show.youtube_id, page_token=next_page_token
        ))

        if response.status_code == status.HTTP_200_OK:
            items = response.data['videos']
            print(response.data)

            for item in items:
                print(item)
                episode = Episode(
                    name=item['title'],
                    youtube_id=item['id'],
                    description=item['description'],
                    thumbnail=item['thumbnail']['url'],
                    duration=item['duration'],
                    position=item['position'],
                    published_at=parser.parse(item['publishedAt']),
                    show=show
                )

                episode.save()

            if 'nextPageToken' in response.data:
                next_page_token = response.data['nextPageToken']
                self.save_page(client=client, show=show, next_page_token=next_page_token)
