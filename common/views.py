from django.db.models import Q
from rest_framework import generics
from rest_framework import permissions
from rest_framework.response import Response
from rest_framework.views import APIView

from common.models import Channel, Show, Episode
from common.serializers import ChannelSerializer, ShowSerializer, EpisodeSerializer


class HealthApi(APIView):
    """
    View to check api health.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    permission_classes = (permissions.IsAdminUser,)

    def get(self, request, format=None):
        """
        return api health
        """
        return Response({'api_health': 'ok'})


class BaseList(generics.ListAPIView):
    """
        View to check api health.

        * Requires token authentication.
        * Only admin users are able to access this view.
        """
    permission_classes = (permissions.AllowAny,)


class ChannelList(BaseList):
    queryset = Channel.objects.all()
    serializer_class = ChannelSerializer


class ShowList(BaseList):
    serializer_class = ShowSerializer

    def get_queryset(self):

        search = self.request.query_params.get('search', None)

        if search and 'channel_id' in self.kwargs:
            channel_id = self.kwargs['channel_id']
            query = channel_id and Q(name__icontains=search) | Q(description__icontains=search)
            queryset = Show.objects.filter(query)

        elif 'channel_id' in self.kwargs:
            channel_id = self.kwargs['channel_id']
            queryset = Show.objects.filter(channel_id=channel_id)

        else:
            queryset = Show.objects.all()

        return queryset.filter(item_count__gt=0).order_by('-published_at')


class EpisodeList(BaseList):
    serializer_class = EpisodeSerializer

    def get_queryset(self):

        search = self.request.query_params.get('search', None)

        if search and 'channel_id' in self.kwargs:
            channel_id = self.kwargs['channel_id']
            query = Q(show__channel_id=channel_id) & (Q(name__icontains=search) | Q(description__icontains=search))
            queryset = Episode.objects.filter(query)

        elif 'channel_id' in self.kwargs:
            channel_id = self.kwargs['channel_id']
            queryset = Episode.objects.filter(show__channel_id=channel_id)

        elif search and 'show_id' in self.kwargs:
            show_id = self.kwargs['show_id']
            query = Q(show_id=show_id) & (Q(name__icontains=search) | Q(description__icontains=search))
            queryset = Episode.objects.filter(query)

        elif 'show_id' in self.kwargs:
            show_id = self.kwargs['show_id']
            queryset = Episode.objects.filter(show_id=show_id)

        else:
            queryset = Episode.objects.all()

        return queryset.filter(show__item_count__gt=0).order_by('-published_at')
