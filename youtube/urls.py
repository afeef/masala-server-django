# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.conf.urls import url
from . import views

urlpatterns = [
    # URL pattern for the UserListView
    # url(
    #     regex=r'^search/(?P<query>.+)/$',
    #     view=views.SearchApi.as_view(),
    #     name='search_api'
    # ),

    url(
        regex=r'^channels/(?P<channel_id>.+)/play-lists/$',
        view=views.PlayListsApi.as_view(),
        name='play_lists_api'
    ),

    url(
        regex=r'^channels/(?P<channel_id>.+)/videos/$',
        view=views.VideosApi.as_view(),
        name='videos_api'
    ),

    url(
        regex=r'^play-lists/(?P<playlist_id>.+)/playlist-items/$',
        view=views.PlayListItemsApi.as_view(),
        name='play_list_items_api'
    ),

    url(
        regex=r'^channels/(?P<username>.+)/$',
        view=views.ChannelsApi.as_view(),
        name='channels_api'
    ),
]
