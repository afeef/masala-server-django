from datetime import datetime

import collections
from django.db import models
from django.utils.translation import ugettext_lazy as _
from jsonfield import JSONField
from taggit.managers import TaggableManager


class BaseModel(models.Model):
    """
    Base model with PK primary key
    """
    modified_at = models.DateTimeField(default=datetime.today)
    created_at = models.DateTimeField(default=datetime.today)

    class Meta:
        abstract = True
        ordering = ('pk',)

    def __str__(self):
        if self.__dict__.get("name"):
            return self.name
        else:
            return self.pk

    def save(self, *args, **kwargs):
        # Update timestamp
        if not self.pk:
            self.created_at = datetime.today()
        self.modified_at = datetime.today()

        return super(BaseModel, self).save(*args, **kwargs)


class Channel(BaseModel):
    youtube_id = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, default=_('New channel'))
    description = models.CharField(max_length=1000, blank=True, null=True)
    thumbnail = models.URLField(max_length=255, blank=True, null=True)
    published_at = models.DateTimeField(blank=True, null=True)

    class Meta(BaseModel.Meta):
        ordering = ['name']


class Chef(BaseModel):
    name = models.CharField(max_length=255, default=_('New playlist'))
    description = models.CharField(max_length=1000, blank=True, null=True)
    thumbnail = models.URLField(max_length=255, blank=True, null=True)


class Show(BaseModel):
    youtube_id = models.CharField(max_length=255, null=True)
    name = models.CharField(max_length=255, default=_('New playlist'))
    description = models.CharField(max_length=1000, blank=True, null=True)
    thumbnail = models.URLField(max_length=255, blank=True, null=True)
    published_at = models.DateTimeField(blank=True, null=True)
    item_count = models.IntegerField(blank=True, null=True)
    channel = models.ForeignKey('Channel', related_name='shows', blank=True, null=True)
    chef = models.ForeignKey('Chef', related_name='shows', blank=True, null=True)
    tags = TaggableManager()


class Episode(BaseModel):
    youtube_id = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, default=_('New playlist'))
    description = models.CharField(max_length=2000, blank=True, null=True)
    thumbnail = models.URLField(max_length=255, blank=True, null=True)
    duration = models.IntegerField(blank=True, null=True)
    position = models.IntegerField(blank=True, null=True)
    published_at = models.DateTimeField(blank=True, null=True)
    show = models.ForeignKey('Show', related_name='episodes', blank=True, null=True)
    chef = models.ForeignKey('Chef', related_name='episodes', blank=True, null=True)
    tags = TaggableManager()


class Recipe(BaseModel):
    youtube_id = models.CharField(max_length=255, blank=True, null=True)
    name = models.CharField(max_length=255, default=_('New playlist'))
    description = models.CharField(max_length=1000, blank=True, null=True)
    thumbnail = models.URLField(max_length=255, blank=True, null=True)
    published_at = models.DateTimeField(blank=True, null=True)
    episode = models.ForeignKey('Episode', related_name='recipes', blank=True, null=True)
    ingredients = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict}, blank=True, null=True)
    directions = JSONField(load_kwargs={'object_pairs_hook': collections.OrderedDict}, blank=True, null=True)
    tags = TaggableManager()

