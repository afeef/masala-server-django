from django.contrib import admin

from common.models import Channel, Chef, Show, Episode, Recipe


class BaseAdmin(admin.ModelAdmin):
    """
    Base class for admin objects
    """
    show_fields = ()
    use_fields = ()

    def __init__(self, *args, **kwargs):
        return super(BaseAdmin, self).__init__(*args, **kwargs)


@admin.register(Channel)
class ChannelAdmin(BaseAdmin):
    """
    Channel admin
    """
    model = Channel
    list_display = ('pk', 'name', 'description', 'published_at', 'modified_at')
    search_fields = ('youtube_id', 'name')
    fields = ('youtube_id', 'name', 'description', 'thumbnail', 'published_at',)


@admin.register(Chef)
class ChefAdmin(BaseAdmin):
    """
    Chef admin
    """
    model = Channel
    list_display = ('pk', 'name', 'description', 'modified_at')
    search_fields = ('name',)
    fields = ('name', 'description', 'thumbnail',)


@admin.register(Show)
class ShowAdmin(BaseAdmin):
    """
    Show admin
    """
    model = Show
    list_display = ('pk', 'name', 'channel', 'chef', 'published_at', 'item_count', 'modified_at')
    search_fields = ('youtube_id', 'name', 'chef__name', 'tags',)
    fields = ('youtube_id', 'name', 'description', 'thumbnail', 'published_at', 'item_count', 'channel', 'chef', 'tags',)


@admin.register(Episode)
class EpisodeModel(BaseAdmin):
    """
    Episode admin
    """
    model = Episode
    list_display = ('pk', 'name', 'show', 'chef', 'published_at', 'modified_at')
    search_fields = ('name', 'show__name', 'chef__name', 'tags',)
    fields = ('name', 'description', 'thumbnail', 'published_at', 'show', 'chef', 'tags',)


@admin.register(Recipe)
class RecipeModel(BaseAdmin):
    """
    Recipe admin
    """
    model = Recipe
    list_display = ('pk', 'name', 'episode', 'published_at', 'modified_at')
    search_fields = ('youtube_id', 'name', 'episode__name', 'tags',)
    fields = (
        'youtube_id',
        'name',
        'description',
        'thumbnail',
        'published_at',
        'episode',
        'ingredients',
        'directions',
        'tags',
    )
