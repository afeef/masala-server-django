import datetime
import isodate
import timeago
from dateutil import parser
from django.conf import settings
from googleapiclient.discovery import build
from rest_framework import permissions
from rest_framework.views import APIView
from rest_framework.response import Response


class BaseApiView(APIView):
    """
        View to check api health.

        * Requires token authentication.
        * Only admin users are able to access this view.
        """
    permission_classes = (permissions.AllowAny,)

    @staticmethod
    def get_authenticated_service():
        return build(settings.YOUTUBE_API_SERVICE_NAME, settings.YOUTUBE_API_VERSION,
                     developerKey=settings.YOUTUBE_DEVELOPER_KEY)


class ChannelsApi(BaseApiView):
    def get(self, request, username):
        """
        Get all channels of user
        """
        youtube = self.get_authenticated_service()
        youtube_response = youtube.channels().list(
            part='snippet,contentDetails',
            forUsername=username
        ).execute()

        return Response(youtube_response)


class PlayListsApi(BaseApiView):
    def get(self, request, channel_id):
        """
        search all play lists of channel
        """
        query = request.query_params.get('query', None)
        page_token = request.query_params.get('page_token', None)

        if query:
            youtube_response = self.search_play_lists(channel_id=channel_id, query=query, page_token=page_token)
        else:
            youtube_response = self.get_play_lists(channel_id=channel_id, page_token=page_token)

        return Response(youtube_response)

    def search_play_lists(self, channel_id, query, page_token):

        youtube = self.get_authenticated_service()
        response = youtube.search().list(
            q=query,
            type='playlist',
            part='snippet',
            channelId=channel_id,
            maxResults=50,
            pageToken=page_token
        ).execute()

        play_list_ids = [item['id']['playlistId'] for item in response['items']]

        return self.get_play_lists(channel_id=None, page_token=page_token, play_list_ids=play_list_ids)

    def get_play_lists(self, channel_id, page_token, play_list_ids=None):
        youtube = self.get_authenticated_service()
        ids = None

        if play_list_ids:
            ids = ','.join(play_list_ids)

        response = youtube.playlists().list(
            part='snippet,contentDetails',
            maxResults=50,
            channelId=channel_id,  # check for existance
            pageToken=page_token,  # check for existance
            id=ids,  # check for existance
        ).execute()

        play_lists = [self.filter_response(item) for item in response['items']]

        response['play_lists'] = play_lists
        del response['items']

        return response

    @staticmethod
    def filter_response(item):
        playlist = item['snippet']
        playlist['id'] = item['id']
        playlist['kind'] = item['kind']
        playlist['etag'] = item['etag']

        if 'standard' in item['snippet']['thumbnails']:
            playlist['thumbnail'] = item['snippet']['thumbnails']['standard']
        elif 'high' in item['snippet']['thumbnails']:
            playlist['thumbnail'] = item['snippet']['thumbnails']['high']
        else:
            playlist['thumbnail'] = item['snippet']['thumbnails']['default']

        playlist['itemCount'] = item['contentDetails']['itemCount']

        del playlist['thumbnails']

        return playlist


class VideosApi(BaseApiView):
    def __init__(self):
        self.videos = None

    def get(self, request, channel_id):
        """
        search all play lists of channel
        """
        query = request.query_params.get('query', None)
        page_token = request.query_params.get('page_token', None)

        youtube_response = self.search_videos(channel_id=channel_id, query=query, page_token=page_token)

        return Response(youtube_response)

    def search_videos(self, channel_id, query, page_token):
        youtube = self.get_authenticated_service()
        response = youtube.search().list(
            q=query,
            type='video',
            part='snippet',
            channelId=channel_id,
            maxResults=50,
            pageToken=page_token
        ).execute()

        video_ids = [item['id']['videoId'] for item in response['items']]
        self.videos = [self.filter_response(item) for item in response['items']]

        return self.get_videos(channel_id=None, page_token=page_token, video_ids=video_ids)

    def get_videos(self, channel_id=None, page_token=None, video_ids=None):
        youtube = self.get_authenticated_service()
        ids = None

        if video_ids:
            ids = ','.join(video_ids)

        response = youtube.videos().list(
            part='statistics,contentDetails',
            maxResults=50,
            id=ids,
        ).execute()

        for video in self.videos:
            stat = self.find_stat(video, response['items'])
            duration = stat['contentDetails']['duration']  # convert to seconds
            video['duration'] = isodate.parse_duration(duration).total_seconds()
            video['stats'] = stat['statistics']

        response['videos'] = self.videos
        del response['items']

        return response

    @staticmethod
    def find_stat(video, video_stats):
        for stat in video_stats:
            if video['id'] == stat['id']:
                return stat
            else:
                continue

    @staticmethod
    def filter_response(item):
        video = item['snippet']
        video['id'] = item['id']['videoId']
        video['kind'] = item['kind']
        video['etag'] = item['etag']

        # then = parser.parse(item['snippet']['publishedAt']).replace(tzinfo=None)
        # now = datetime.datetime.now()
        # time_delta = now - then
        #
        # video['publishedAt'] = timeago.format(time_delta, 'en_US')

        if 'standard' in item['snippet']['thumbnails']:
            video['thumbnail'] = item['snippet']['thumbnails']['standard']
        elif 'high' in item['snippet']['thumbnails']:
            video['thumbnail'] = item['snippet']['thumbnails']['high']
        else:
            video['thumbnail'] = item['snippet']['thumbnails']['default']

            video['itemCount'] = item['contentDetails']['itemCount']

        del video['thumbnails']

        return video


class PlayListItemsApi(VideosApi):
    def get(self, request, playlist_id):
        """
        search all play lists of channel
        """
        page_token = request.query_params.get('page_token', None)

        youtube_response = self.get_play_list_items(playlist_id=playlist_id, page_token=page_token)

        return Response(youtube_response)

    def get_play_list_items(self, playlist_id, page_token):
        youtube = self.get_authenticated_service()
        response = youtube.playlistItems().list(
            part='snippet',
            playlistId=playlist_id,
            maxResults=50,
            pageToken=page_token
        ).execute()

        video_ids = [item['snippet']['resourceId']['videoId'] for item in response['items']]
        self.videos = [self.filter_response(item) for item in response['items']]

        return self.get_videos(channel_id=None, page_token=page_token, video_ids=video_ids)

    @staticmethod
    def filter_response(item):
        video = item['snippet']
        video['id'] = item['snippet']['resourceId']['videoId']
        video['kind'] = item['kind']
        video['etag'] = item['etag']

        # then = parser.parse(item['snippet']['publishedAt']).replace(tzinfo=None)
        # now = datetime.datetime.now()
        # time_delta = now - then
        #
        # video['publishedAt'] = timeago.format(time_delta, 'en_US')

        if 'standard' in item['snippet']['thumbnails']:
            video['thumbnail'] = item['snippet']['thumbnails']['standard']
        elif 'high' in item['snippet']['thumbnails']:
            video['thumbnail'] = item['snippet']['thumbnails']['high']
        else:
            video['thumbnail'] = item['snippet']['thumbnails']['default']

            video['itemCount'] = item['contentDetails']['itemCount']

        del video['thumbnails']

        return video
