from rest_framework import permissions
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework import response, schemas
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_swagger.renderers import OpenAPIRenderer, SwaggerUIRenderer


@api_view()
@renderer_classes([OpenAPIRenderer, SwaggerUIRenderer])
@permission_classes((permissions.IsAdminUser,))
def schema_view(request):
    generator = schemas.SchemaGenerator(title='Recipes API')
    return response.Response(generator.get_schema(request=request))
