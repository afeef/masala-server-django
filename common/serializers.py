from rest_framework.serializers import ModelSerializer

from common.models import Channel, Show, Episode, Chef, Recipe


class ChannelSerializer(ModelSerializer):
    class Meta:
        model = Channel


class ChefSerializer(ModelSerializer):
    class Meta:
        model = Chef


class ShowSerializer(ModelSerializer):
    class Meta:
        model = Show
        depth = 1


class EpisodeSerializer(ModelSerializer):
    class Meta:
        model = Episode
        depth = 2


class RecipeSerializer(ModelSerializer):
    class Meta:
        model = Recipe
