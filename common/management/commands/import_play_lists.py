from dateutil import parser
from django.core.management.base import BaseCommand
from django.db import transaction
from rest_framework import status
from rest_framework.test import APIClient

from common.models import Channel, Show


@transaction.atomic
class Command(BaseCommand):
    """
    Usage: python manage.py import_play_lists UClkIneW_r2Sp2mjdFt2X4rg
    """
    args = '<channel_id>'
    help = 'Imports all play lists of the given channel_id'

    def handle(self, *args, **options):
        for channel_id in args:

            self.stdout.write('Reading channel {channel_id}'.format(
                channel_id=channel_id
            ))

            if Channel.objects.filter(youtube_id=channel_id).exists():
                channel = Channel.objects.get(youtube_id=channel_id)
                client = APIClient()
                self.save_page(client=client, channel=channel)
            else:
                self.stdout.write('Channel "{channel_id}" not exists.'.format(channel_id=channel.youtube_id))

    def save_page(self, client, channel, next_page_token=''):
        response = client.get('/youtube/channels/{channel_id}/play-lists/?page_token={page_token}'.format(
            channel_id=channel.youtube_id, page_token=next_page_token
        ))

        if response.status_code == status.HTTP_200_OK:
            items = response.data['play_lists']

            for item in items:
                print(item)
                show = Show()
                show.name = item['title']
                show.youtube_id = item['id']
                show.description = item['description']
                show.thumbnail = item['thumbnail']['url']
                show.published_at = parser.parse(item['publishedAt'])
                show.item_count = int(item['itemCount'])
                show.channel = channel
                show.save()

            if 'nextPageToken' in response.data:
                next_page_token = response.data['nextPageToken']
                self.save_page(client=client, channel=channel, next_page_token=next_page_token)
